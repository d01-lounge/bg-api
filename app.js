'use strict';

// Base dir
global.__base = __dirname + '/';

require("nodejs-dashboard");
const glob = require('glob');

// Config
const config = require('./config');
if (process.env.NODE_ENV !== 'test') {
  process.env.NODE_ENV = config.environment.toLowerCase() || 'development';
}

// Dependencies
const express = require('express');
const db = require('./app/middleware/db');
const logger = require('./app/middleware/logger');

// Create Express app
const app = express();
app.disable('x-powered-by');

// Database
db.connectMongoose(config.db.mongo);
// All environments Express middleware
app.set('port', process.env.PORT || config.port);

app.use(require('helmet')());
app.use(require('body-parser').json());
// Use compression to save bandwidth
if (app.get('env') !== 'test') {
  app.use(require('compression')());
}

// Include payload formatting
app.use(require('./app/middleware/response')());

// Error handling middleware should be loaded after the loading the routes
if (app.get('env') === 'development') {
  app.use(logger.middleware);
  app.use(require('errorhandler')());
}

// Include routes if exists
const routes = glob.sync('./app/routes/*.js');
routes.forEach(function onEach(file) {
  require(file)(app);
});

// Response middleware and error handling
app.use(require('./app/middleware/error').middleware());

// Listen
app.listen(app.get('port'), function onListen() {
  const chalk = require('chalk');
  const flag = chalk.white.bgYellow.bold(' BG ');
  const msg = chalk.grey.bgBlack(` ▶ Express server listening on port ${chalk.white(app.get('port'))} `);
  logger.log(`${flag}${msg}`);
});