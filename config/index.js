'use strict';

// Base directory fallback
const logger = require('./../app/middleware/logger');
__base = global.__base || __base;
if (__base === undefined) {
  __base = process.cwd() + '/';
}

const basedir = __base;
const yamlConfig = require('node-yaml-config');
const fs = require('fs');

// Config
const configFile = `${basedir}config/apps.local.yml`;
if (!fs.existsSync(configFile)) {
  logger.log(`${configFile} not found, exiting.`);
  process.exit(0);
}

const config = yamlConfig.load(configFile);

// Database config
const dbConfigFile = `${basedir}config/database.local.yml`;
if (!fs.existsSync(dbConfigFile)) {
  logger.log(dbConfigFile + ' not found, exiting.');
  process.exit(0);
}

config.db = yamlConfig.load(dbConfigFile);

// App config
const appConfigFile = `${basedir}config/app.json`;
if (fs.existsSync(appConfigFile)) {
  config.app = require(appConfigFile);
}

exports = module.exports = config;
