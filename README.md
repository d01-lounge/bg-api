bright-giraffe
==================

## requirements

* node 4.4
* npm
*

## setup

`npm install` before running

## Testing

`npm test` will lint and test your code

## Start Development Server

`npm start` will start a development server by default on [http://localhost:3000](http://localhost:3000)

## ES6

enabled, though keep an eye on supported features up till node 4.4

## Roadmap

* security
* domain / organisation / group
* flesh out mailing list entity with memberships
* combine events (recurring)
* schedule mails
* generate mails
* send mails
* generate ical
* GraphQL