'use strict';

/* eslint-disable*/
// Dependencies
var expect = require('chai').expect;
var supertest = require('supertest');

// Start the application
require('../../app.js');

// Config
var config = require('./../../config');
var api = supertest('http://localhost:' + config.port);

// Database
var mongoose = require('./../../app/middleware/db').mongoose;
var cleanMongo = require('../mocks/cleanMongo');

// Endpoint to test
var endpoint = '/api/v1/examples';

describe('Example API', function onTestExample() {
  /**
   * Dummy test
   */
  it('Dummy test', function () {
    expect(true).to.equal(true);
  })
});
