'use strict';

// Environment
process.env.NODE_ENV = 'test';

// Dependencies
var expect = require('chai').expect;
var supertest = require('supertest');

// Start the application
require('../../app.js');

// Config
var config = require('./../../config');
var api = supertest('http://localhost:' + config.port);

// Database
var cleanMongo = require('../mocks/cleanMongo');
var Event = require('../../app/models/event');
var List = require('../../app/models/list');

// Endpoint to test
var endpoint = '/api/v1/events';

let hostList;

describe('Event API', function onTest() {

  function createEvent(postData, callback) {
    var newDoc = new Event(postData);
    newDoc.createdBy = '5314a68e3a51f5ee5e000005';
    newDoc.start = newDoc.start || Date.now();
    newDoc.save(callback);
  }

  function createList(next) {
    var newList = new List({
      name: 'host for the testEvents',
      createdBy: '5314a68e3a51f5ee5e000005',
      slug: 'hostlist-for-testevents'
    });

    newList.save((err, doc) => {
      hostList = doc;
      next();
    });
  }

  beforeEach(createList);
  afterEach(cleanMongo);
  afterEach(next => {
    hostList = undefined;
    next();
  })

  /**
   * Create event
   */
  describe('POST /events', function onPost() {
    it('should create a event', function onIt(done) {
      var postData = {
        title: 'Event title',
        createdBy: '5314a68e3a51f5ee5e000005',
        start: Date.now(),
        mailingList: hostList._id
      };

      api.post(endpoint)
        .send(postData)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function onResponse(err, res) {
          if (err) {
            return done(err);
          }
          expect(res.body.title).to.equal(postData.title);
          done(err);
        });
    });

    it('should return a bad request when no creator was provided', function onIt(done) {
      var postData = {
        title: 'value',
        start: Date.now()
      };

      api.post(endpoint)
        .send(postData)
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

    it('should return a bad request when no startdate was provided', function onIt(done) {
      var postData = {
        title: 'value',
        createdBy: '5314a68e3a51f5ee5e000005'
      };

      api.post(endpoint)
        .send(postData)
        .expect('Content-Type', /json/)
        .expect(400, done);
    });


    it('should return a bad request when no title was provided', function onIt(done) {
      var postData = {
        createdBy: '5314a68e3a51f5ee5e000005',
        start: Date.now()
      };

      api.post(endpoint)
        .send(postData)
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

  });

  /**
   * Read event
   */
  describe('GET /events/:eventId', function onGet() {
    var event = {};

    beforeEach(function onBeforeEach(done) {
      var postData = {
        title: 'Event title',
        start: Date.now(),
        mailingList: hostList
      };
      createEvent(postData, function onCreate(err, doc) {
        event = doc;
        done(err);
      });
    });

    it('should return a event', function onIt(done) {
      api.get(endpoint + '/' + event._id)
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function onResponse(err, res) {
          if (err) {
            return done(err);
          }
          expect(res.body.id).to.equal(event._id.toString());
          expect(res.body.title).to.equal(event.title);
          done(err);
        });
    });

    it('should return error 400 with invalid id', function onIt(done) {
      api.get(endpoint + '/invalidid')
        .set('Accept', 'application/json')
        .expect(400, done);
    });

    it('should return error 404 when event was not found', function onIt(done) {
      api.get(endpoint + '/55af8f473bf0aaadd34c4689')
        .set('Accept', 'application/json')
        .expect(404, done);
    });

  });

  /**
   * Update event
   */
  describe('PUT /events/:eventId', function onPut() {

    var event = {};

    beforeEach(function onBeforeEach(done) {
      var postData = {
        title: 'Event title',
        start: Date.now(),
        mailingList: hostList
      };
      createEvent(postData, function onCreate(err, doc) {
        event = doc;
        done(err);
      });
    });

    it('should return an updated event', function onIt(done) {

      var putData = {
        title: 'value2'
      };

      api.put(endpoint + '/' + event._id)
        .send(putData)
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function onResponse(err, res) {
          if (err) {
            return done(err);
          }
          expect(res.body.id).to.equal(event._id.toString());
          expect(res.body.title).to.equal(putData.title);
          done(err);
        });
    });

    it('should return 400 on invalid event title', function onIt(done) {
      var putData = {
        title: null
      };

      api.put(endpoint + '/' + event._id)
        .send(putData)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

    it('should return 400 on invalid event creator', function onIt(done) {
      var putData = {
        createdBy: null
      };

      api.put(endpoint + '/' + event._id)
        .send(putData)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

    it('should return 400 on invalid event start', function onIt(done) {
      var putData = {
        start: null
      };

      api.put(endpoint + '/' + event._id)
        .send(putData)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

  });


  /**
   * Delete event
   */
  describe('DELETE /events/:eventId', function onDelete() {
    var event = {};

    beforeEach(function onBeforeEach(done) {
      var postData = {
        title: 'Event title',
        start: Date.now(),
        mailingList: hostList
      };

      createEvent(postData, function onCreate(err, doc) {
        event = doc;
        done(err);
      });
    });

    it('should be able to delete a event', function onIt(done) {
      api.delete(endpoint + '/' + event._id)
        .set('Accept', 'application/json')
        .expect(204, done)
    });
  });

  /**
   * List all events
   */
  describe('GET /events', function onList() {
    it('should return events array', function onIt(done) {
      api.get(endpoint)
      .set('Accept', 'application/json')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function onResponse(err, res) {
        if (err) {
          return done(err);
        }
        expect(res.body).to.be.an('array');
        done(err);
      });
    });
  });
});
