'use strict';

// Environment
process.env.NODE_ENV = 'test';

// Dependencies
var expect = require('chai').expect;
var supertest = require('supertest');

// Start the application
require('../../app.js');

// Config
var config = require('./../../config');
var api = supertest('http://localhost:' + config.port);

// Database
var cleanMongo = require('../mocks/cleanMongo');
var User = require('../../app/models/user');

// Endpoint to test
var endpoint = '/api/v1/users';

describe('User API', function onTest() {

  function createUser(postData, callback) {
    var newDoc = new User(postData);
    newDoc.createdBy = '5314a68e3a51f5ee5e000005';
    newDoc.save(callback);
  }

  afterEach(cleanMongo);

  /**
   * Create user
   */
  describe('POST /users', function onPost() {
    it('should create a user', function onIt(done) {
      var postData = {
        email: 'test@test.com',
        name: 'Test Erde Test'
      };

      api.post(endpoint)
        .send(postData)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function onResponse(err, res) {
          if (err) {
            return done(err);
          }
          expect(res.body.email).to.equal(postData.email);
          expect(res.body.name).to.equal(postData.name);
          done(err);
        });
    });

    it('should return a bad request when no email was provided', function onIt(done) {
      var postData = {
        name: 'Test Erde Test'
      };

      api.post(endpoint)
        .send(postData)
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

  });

  /**
   * Read user
   */
  describe('GET /users/:userId', function onGet() {
    var user = {};

    beforeEach(function onBeforeEach(done) {
      var postData = {
        email: 'test@test.com',
        name: 'value'
      };
      createUser(postData, function onCreate(err, doc) {
        user = doc;
        done(err);
      });
    });

    it('should return a user', function onIt(done) {
      api.get(endpoint + '/' + user._id)
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function onResponse(err, res) {
          if (err) {
            return done(err);
          }
          expect(res.body.id).to.equal(user._id.toString());
          expect(res.body.name).to.equal(user.name);
          expect(res.body.email).to.equal(user.email);
          done(err);
        });
    });

    it('should return error 400 with invalid id', function onIt(done) {
      api.get(endpoint + '/invalidid')
        .set('Accept', 'application/json')
        .expect(400, done);
    });

    it('should return error 404 when user was not found', function onIt(done) {
      api.get(endpoint + '/55af8f473bf0aaadd34c4689')
        .set('Accept', 'application/json')
        .expect(404, done);
    });

  });

  /**
   * Update user
   */
  describe('PUT /users/:userId', function onPut() {

    var user = {};

    beforeEach(function onBeforeEach(done) {
      var postData = {
        email: 'test@test.com',
        name: 'Test erde Test'
      };
      createUser(postData, function onCreate(err, doc) {
        user = doc;
        done(err);
      });
    });

    it('should return an updated user', function onIt(done) {

      var putData = {
        name: 'Test New Name Test'
      };

      api.put(endpoint + '/' + user._id)
        .send(putData)
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function onResponse(err, res) {
          if (err) {
            return done(err);
          }
          expect(res.body.id).to.equal(user._id.toString());
          expect(res.body.name).to.equal(putData.name);
          done(err);
        });
    });

    it('should return 400 on invalid user email', function onIt(done) {
      var putData = {
        email: null
      };

      api.put(endpoint + '/' + user._id)
        .send(putData)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

  });


  /**
   * Delete user
   */
  describe('DELETE /users/:userId', function onDelete() {
    var user = {};

    beforeEach(function onBeforeEach(done) {
      var postData = {
        email: 'test@test.com'
      };

      createUser(postData, function onCreate(err, doc) {
        user = doc;
        done(err);
      });
    });

    it('should be able to delete a user', function onIt(done) {
      api.delete(endpoint + '/' + user._id)
        .set('Accept', 'application/json')
        .expect(204, done);
    });
  });

  /**
   * List all users
   */
  describe('GET /users', function onList() {
    it('should return users array', function onIt(done) {
      api.get(endpoint)
      .set('Accept', 'application/json')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function onResponse(err, res) {
        if (err) {
          return done(err);
        }
        expect(res.body).to.be.an('array');
        done(err);
      });
    });
  });
});
