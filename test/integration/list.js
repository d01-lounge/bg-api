'use strict';

// Environment
process.env.NODE_ENV = 'test';

// Dependencies
var expect = require('chai').expect;
var supertest = require('supertest');

// Start the application
require('../../app.js');

// Config
var config = require('./../../config');
var api = supertest('http://localhost:' + config.port);

// Database
var cleanMongo = require('../mocks/cleanMongo');
var List = require('../../app/models/list');

// Endpoint to test
var endpoint = '/api/v1/lists';

describe('List API', function onTest() {

  function createList(postData, callback) {
    var newDoc = new List(postData);
    newDoc.createdBy = '5314a68e3a51f5ee5e000005';
    newDoc.slug = newDoc.name;
    newDoc.save(callback);
  }

  after(cleanMongo);

  /**
   * Create list
   */
  describe('POST /lists', function onPost() {
    it('should create a list', function onIt(done) {
      var postData = {
        name: 'value',
        createdBy: '5314a68e3a51f5ee5e000005'
      };

      api.post(endpoint)
        .send(postData)
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function onResponse(err, res) {
          if (err) {
            return done(err);
          }
          expect(res.body.name).to.equal(postData.name);
          done(err);
        });
    });

    it('should return a bad request when no name is provided', function onIt(done) {
      var postData = {
        createdBy: '5314a68e3a51f5ee5e000005'
      };

      api.post(endpoint)
        .send(postData)
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

    it('should return a bad request when no user is provided', function onIt(done) {
      var postData = {
        name: 'value'
      };

      api.post(endpoint)
        .send(postData)
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

  });

  /**
   * Read list
   */
  describe('GET /lists/:listId', function onGet() {
    var list = {};

    beforeEach(function onBeforeEach(done) {
      var postData = {
        name: 'value'
      };
      createList(postData, function onCreate(err, doc) {
        list = doc;
        done(err);
      });
    });

    it('should return a list', function onIt(done) {
      api.get(endpoint + '/' + list._id)
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function onResponse(err, res) {
          if (err) {
            return done(err);
          }
          expect(res.body.id).to.equal(list._id.toString());
          expect(res.body.name).to.equal(list.name);
          done(err);
        });
    });

    it('should return error 400 with invalid id', function onIt(done) {
      api.get(endpoint + '/invalidid')
        .set('Accept', 'application/json')
        .expect(400, done);
    });

    it('should return error 404 when list was not found', function onIt(done) {
      api.get(endpoint + '/55af8f473bf0aaadd34c4689')
        .set('Accept', 'application/json')
        .expect(404, done);
    });

  });

  /**
   * Update list
   */
  describe('PUT /lists/:listId', function onPut() {

    var list = {};

    beforeEach(function onBeforeEach(done) {
      var postData = {
        name: 'value'
      };
      createList(postData, function onCreate(err, doc) {
        list = doc;
        done(err);
      });
    });

    it('should return an updated list', function onIt(done) {

      var putData = {
        name: 'value2'
      };

      api.put(endpoint + '/' + list._id)
        .send(putData)
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function onResponse(err, res) {
          if (err) {
            return done(err);
          }
          expect(res.body.id).to.equal(list._id.toString());
          expect(res.body.name).to.equal(putData.name);
          done(err);
        });
    });

    it('should return 400 on invalid list name', function onIt(done) {
      var putData = {
        name: null
      };

      api.put(endpoint + '/' + list._id)
        .send(putData)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

    it('should return 400 on invalid list creator', function onIt(done) {
      var putData = {
        createdBy: null
      };

      api.put(endpoint + '/' + list._id)
        .send(putData)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400, done);
    });

  });


  /**
   * Delete list
   */
  describe('DELETE /lists/:listId', function onDelete() {
    var list = {};

    beforeEach(function onBeforeEach(done) {
      var postData = {
        name: 'value'
      };

      createList(postData, function onCreate(err, doc) {
        list = doc;
        done(err);
      });
    });

    it('should be able to delete a list', function onIt(done) {
      api.delete(endpoint + '/' + list._id)
        .set('Accept', 'application/json')
        .expect(204, done);
    });
  });

  /**
   * List all lists
   */
  describe('GET /lists', function onList() {
    it('should return lists array', function onIt(done) {
      api.get(endpoint)
      .set('Accept', 'application/json')
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function onResponse(err, res) {
        if (err) {
          return done(err);
        }
        expect(res.body).to.be.an('array');
        done(err);
      });
    });
  });
});
