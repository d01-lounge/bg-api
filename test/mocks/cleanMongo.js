'use strict';

var async = require('async');

require('../../app');
var mongoose = require('../../app/middleware/db').mongoose;

function cleanMongo(next) {

  var collections = Object.keys(mongoose.connection.collections);

  async.forEach(collections, function onEach(collectionName, done) {

    mongoose.connection.collections[collectionName].drop(function onDrop(err) {

      if (err && err.message !== 'ns not found') {
        return done(err);
      }

      done(null);

    });

  }, next);

}

before(cleanMongo);
after(cleanMongo);

module.exports = cleanMongo;
