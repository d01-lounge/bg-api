'use strict';

module.exports = (arrArgument) => {
  return arrArgument.filter((element, position, arr) => {
    return arr.indexOf(element) == position;
  });
};