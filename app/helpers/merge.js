'use strict';

/**
 * Merge two objects
 * @param {Object} destination
 * @param {Object} source
 */
function merge(destination, source) {

  for (var key in source) {

    if (Array.isArray(source[key])) {
      destination[key] = source[key].slice();
    } else if (destination[key] && typeof destination[key] === 'object') {
      merge(destination[key], source[key]);
    } else {
      destination[key] = source[key];
    }

  }

}

module.exports = merge;
