'use strict';

/**
 * Service to interact with the layer collection
 * @module services/feature
 */
var List = require('../models/list');

function returnCheckedCallbackResult(saveError, saveList, numOfSavedLists, callback) {
  if (saveError) {
    return callback(saveError);
  }
  return callback(null, saveList);
}

/**
 *Lists subscriber
 * @param subscriberId
 * @param subscriber
 * @param callback
 */
function addSubscriber(listId, subscriberId, callback) {

  List.findById(listId, function foundList(err, list) {
    if (err) {
      return callback(err);
    }

    if (!list) {
      return callback(new Error(`Could not find list with id=${listId}`));
    }
    var filtered = list.subscribers.filter((x) => {
      return x.toString() === subscriberId;
    });

    if (filtered.length > 0) {
      return callback(new Error(`Subscriber ${subscriberId} is already member of list ${listId}`))
    }

    list.subscribers.push(subscriberId);
    list.save(function onSave(saveError, saveList, numOfSavedLists) {
      returnCheckedCallbackResult(saveError, saveList, numOfSavedLists, callback);
    });

  });
}

/**
 * Delete subscriber
 * @param listId
 * @param subscriberId
 * @param callback
 */
function deleteSubscriber(listId, subscriberId, callback) {

  List.findById(listId, function foundList(err, list) {
    if (err) {
      return callback(err);
    }

    if (!list) {
      return callback(new Error('Could not find list with id=' + listId));
    }

    list.subscribers = list.subscribers.filter(function onFilter(subscriber) {
      return subscriber._id.toString() !== subscriberId;
    });

    list.save(function onSave(saveError, saveList, numOfSavedLists) {
      returnCheckedCallbackResult(saveError, saveList, numOfSavedLists, callback);
    });

  });

}


module.exports = {
  addSubscriber,
  deleteSubscriber
};
