'use strict';

const mongoose = require('mongoose');
const chalk = require('chalk');
const logger = require('./../logger');

function createConnection(options) {

  mongoose.connect(options.uri, {
    server: {
      auto_reconnect: false,
      socketOptions: {
        keepAlive: 1800,
        connectTimeoutMS: 30000
      },
      strategy: 'ping',
      readPreference: 'nearest'
    },
    replset: {
      socketOptions: {
        keepAlive: 1800,
        connectTimeoutMS: 30000
      },
      strategy: 'ping',
      readPreference: 'nearest'
    }
  });

  if (options.debug) {
    mongoose.set('debug', true);
  }

  mongoose.connection.on('connected', function onConnected() {
    logger.log(chalk.white.bgYellow.bold(' BG ') + chalk.gray.bgBlack(' ▶ Mongoose connected'));
  });

  mongoose.connection.on('error', function onError(err) {
    logger.log(chalk.white.bgYellow.bold(' BG ') + chalk.gray.bgBlack(' ▶ ! Mongoose error: ' + err));
  });

  mongoose.connection.on('disconnected', function onDisconnected() {
    logger.log(chalk.white.bgYellow.bold(' BG ') + chalk.gray.bgBlack(' ▶ ! Mongoose disconnected'));
  });

  process.on('SIGINT', function onSigint() {
    mongoose.connection.close(function onClose() {
      process.exit(0);
    });
  });

  return mongoose;

}

module.exports = {
  mongoose: mongoose,
  createConnection: createConnection
};