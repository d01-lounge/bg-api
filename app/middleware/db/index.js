'use strict';

const mongo = require('./mongo');

module.exports = {
  mongoose: mongo.mongoose,
  connectMongoose: mongo.createConnection
};