'use strict';

const logger = require('./logger.js');
const middleware = require('./middleware');

module.exports = Object.assign({ middleware }, logger);
