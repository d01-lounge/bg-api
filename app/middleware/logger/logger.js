'use strict';

const winston = require('winston');
const util = require('util');

let loglevel = 'info';

if (process.env.NODE_ENV && process.env.NODE_ENV.toLowerCase() === 'development') {
  loglevel = 'debug';
}

const timestamp = () => new Date().toISOString();

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      name: 'console',
      level: loglevel,
      formatter: (options) => `${options.level.toUpperCase()}: ${timestamp()}: ${options.message}`
    })
  ]
});

logger.add(require('winston-daily-rotate-file'), {
  name: 'error-file',
  filename: 'logs/error.log',
  level: 'error'
});

function displayMessage(message) {
  if (typeof message === 'object') {
    return util.inspect(message);
  }
  return message;
}

const debug = (message) => logger.log('debug', displayMessage(message));
const info = (message) => logger.log('info', displayMessage(message));
const warn = (message) => logger.log('warn', displayMessage(message));
const error = (message) => logger.log('error', displayMessage(message));

module.exports = {
  debug,
  warn,
  error,
  info,
  log: info
};