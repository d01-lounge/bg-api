'use strict';

const isNumber = require('isnumber');
const errors = require('errors');

function middleware() {
  return function returnMiddleware(req, res, next) {

    res.success = function onSuccess(_status, _data) {
      let status = _status;
      let data = _data;

      if (!isNumber(status)) {
        data = status;
        status = 200;
      }

      if (!data) {
        status = 204;
      }

      res.status(status).json(data);
    };

    res.error = function onError(err) {
      res.status(err.status || 500).json(err);
    };

    res.sendResponse = function onSendResponse(defaultError, doc, _cb) {
      let callback = _cb;

      if (typeof defaultError !== 'string' && !(defaultError instanceof Error)) {
        callback = defaultError;
      }
      if (typeof doc === 'function') {
        callback = doc;
      }

      return function onProcess(err, _result) {
        let result = _result;
        if (err) {
          return callback(err || defaultError);
        }
        if (!result) {
          if (typeof doc === 'object') {
            result = doc;
          } else {
            return next(new errors.NotFoundError());
          }
        }

        return res.success(result);
      };
    };

    next();

  };
}

module.exports = middleware;
