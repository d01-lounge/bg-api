'use strict';

const errors = require('errors');

// error handling middleware
function errorHandler() {
  return function middleware(err, req, res, next) {
    if (!err) {
      return next();
    }

    // Fall back on the mapped statusses for specific errors
    switch (err.name) {
    case 'CastError':
    case 'ValidationError':
    case 'BadRequestError':
      return res.error(new errors.Http400Error(err.message));
    case 'NotFoundError':
      return res.error(new errors.Http404Error(err.message));
    case 'TypeError':
    case 'Error':
    default:
      return res.error(err);
    }
  };
}

// default errors
errors.create({ name: 'UnauthorizedError', status: 401, defaultMessage: 'Unauthorized' });
errors.create({ name: 'ForbiddenError', status: 403, defaultMessage: 'Forbidden' });
errors.create({ name: 'NotFoundError', status: 404, defaultMessage: 'Not found' });
errors.create({ name: 'BadRequestError', status: 400 });
errors.create({ name: 'ValidationError', status: 400 });

module.exports = (() => {
  errors.middleware = errorHandler;
  return errors;
})();