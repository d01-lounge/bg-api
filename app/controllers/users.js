'use strict';

const deepExtend = require('deep-extend');
const errors = require('../middleware/error');
const User = require('../models/user');

function create(req, res, next) {
  const user = new User(req.body);
  user.save((err, result) => {
    if (err) {
      return next(err);
    }
    return res.success(result);
  });
}

function read(req, res) {
  res.success(req.user);
}

function update(req, res, next) {
  const user = req.user;
  const updatedUser = deepExtend(user, req.body);

  updatedUser.save((err, result) => {
    if (err) {
      return next(err);
    }
    return res.success(result);
  });
}

function remove(req, res, next) {
  const user = req.user;
  user.remove((err) => {
    if (err) {
      return next(err);
    }
    return res.success();
  });
}

function list(req, res, next) {
  User.find().exec((err, users) => {
    if (err) {
      return next(err);
    }
    return res.success(users);
  });
}

function findById(req, res, next, id) {
  User.findById(id, (err, user) => {
    if (err) {
      return next(err);
    }

    if (!user) {
      return res.error(new errors.NotFoundError());
    }

    req.user = user;
    return next();
  });
}

module.exports = {
  create,
  read,
  update,
  remove,
  list,
  findById
};
