'use strict';

const deepExtend = require('deep-extend');
const errors = require('../middleware/error');
const Event = require('../models/event');

function create(req, res, next) {
  var event = new Event(req.body);
  event.save((err, result) => {
    if (err) {
      return next(err);
    }
    return res.success(result);
  });
}

function read(req, res) {
  res.success(req.event);
}

function update(req, res, next) {
  const event = req.event;
  const updatedEvent = deepExtend(event, req.body);
  updatedEvent.save((err, result) => {
    if (err) {
      return next(err);
    }
    return res.success(result);
  });
}

function remove(req, res, next) {
  const event = req.event;
  event.remove((err) => {
    if (err) {
      return next(err);
    }
    return res.success();
  });
}

function list(req, res, next) {
  Event
    .find()
    .exec((err, events) => {
      if (err) {
        return next(err);
      }
      return res.success(events);
    });
}

function findById(req, res, next, id) {
  Event
    .findById(id)
    .exec((err, event) => {
      if (err) {
        return next(err);
      }

      if (!event) {
        return res.error(new errors.NotFoundError());
      }

      req.event = event;
      return next();
    });
}

module.exports = {
  create,
  read,
  update,
  remove,
  list,
  findById
};