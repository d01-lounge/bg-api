'use strict';

const errors = require('../middleware/error');
const List = require('../models/list');
const slug = require('slug');
const uniqueArray = require('./../helpers/unique');
const listService = require('../services/list');
const async = require('async');

const sluggify = (str) => slug(str, {lower: true});

function create(req, res, next) {
  // TODO: set creator to the logged in user.
  const body = Object.assign({
    subscribers: []
  }, req.body);

  if (!body.name) {
    return next(new errors.BadRequestError('"name" is required'));
  }

  if (!body.createdBy) {
    return next(new errors.BadRequestError('"createdBy" is required'));
  }


  body.slug = body.slug || sluggify(body.name);

  const newList = new List(body);
  newList.save((err, result) => {
    if (err) {
      return next(err);
    }

    // TODO: repopulate after update?
    return res.success(result);
  });
}

function read(req, res) {
  res.success(req.list);
}

function update(req, res, next) {
  const body = req.body;

  List
    .findById(req.params.listId)
    .exec((err, listDoc) => {
      if (err) {
        return next(err);
      }

      if (!listDoc) {
        return res.error(new errors.NotFoundError());
      }

      const listToUpdate = listDoc;
      const updatedList = Object.assign(listToUpdate, body);
      updatedList.slug = updatedList.slug || sluggify(updatedList.name);
      updatedList.save((err, result) => {
        if (err) {
          return next(err);
        }

        // TODO: repopulate after update?
        return res.success(result);
      });
    });
}

function remove(req, res, next) {
  const listToRemove = req.list;
  listToRemove.remove((err) => {
    if (err) {
      return next(err);
    }
    return res.success();
  });
}

function list(req, res, next) {
  List
    .find()
    .populate('createdBy', 'name email')
    .populate('subscribers', 'name')
    .exec((err, lists) => {
      if (err) {
        return next(err);
      }
      res.success(lists);
    });
}

function findById(req, res, next, id) {
  List
    .findById(id)
    .populate('createdBy', 'name email')
    .populate('subscribers', 'name')
    .exec((err, listDoc) => {
      if (err) {
        return next(err);
      }

      if (!listDoc) {
        return res.error(new errors.NotFoundError());
      }

      req.list = listDoc;
      return next();
    });
}

function addSubscriber(req, res, next) {
  const subscriberId = req.params.subscriberId;
  const listId = req.params.listId;
  listService.addSubscriber(listId, subscriberId, (err, result) => {
    if (err) {
      return next(err);
    }

    return res.success(result);
  });
}

function removeSubscriber(req, res, next) {
  const subscriberId = req.params.subscriberId;
  const listId = req.params.listId;
  listService.removeSubscriber(listId, subscriberId, (err, result) => {
    if (err) {
      return next(err);
    }

    return res.success(result);
  });
}

module.exports = {
  create,
  read,
  update,
  remove,
  list,
  findById,
  addSubscriber,
  removeSubscriber
};
