'use strict';

const express = require('express');
const lists = require('../controllers/lists');

module.exports = function getRouter(app) {

  const router = new express.Router();

  router.route('/')
        .get(lists.list)
        .post(lists.create);

  router.route('/:listId')
        .get(lists.read)
        .put(lists.update)
        .delete(lists.remove);

  router.route('/:listId/subscribers/:subscriberId')
        .post(lists.addSubscriber)
        .delete(lists.removeSubscriber)

  router.param('listId', lists.findById);

  // Register our routes
  app.use('/api/v1/lists', router);
};
