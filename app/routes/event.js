'use strict';

const express = require('express');
const events = require('../controllers/events');

module.exports = function getRouter(app) {

  const router = new express.Router();

  router.route('/')
        .get(events.list)
        .post(events.create);

  router.route('/:eventId')
        .get(events.read)
        .put(events.update)
        .delete(events.remove);

  router.param('eventId', events.findById);

  // Register our routes
  app.use('/api/v1/events', router);
};
