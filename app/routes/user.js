'use strict';

const express = require('express');
const users = require('../controllers/users');

module.exports = function getRouter(app) {

  const router = new express.Router();

  router.route('/')
        .get(users.list)
        .post(users.create);

  router.route('/:userId')
        .get(users.read)
        .put(users.update)
        .delete(users.remove);

  router.param('userId', users.findById);

  // Register our routes
  app.use('/api/v1/users', router);
};
