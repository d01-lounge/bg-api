'use strict';

const mongoose = require('./../middleware/db').mongoose;

const timestamps = require('mongoose-timestamp');

const Schema = mongoose.Schema;

const SchemaHelper = require('./../helpers/schema');

const ListSchema = new Schema({
  name: { type: String, required: true },
  slug: { type: String, required: true },
  description: { type: String, required: false },
  createdBy: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
  subscribers: [ { type: Schema.Types.ObjectId, ref: 'User' } ]
});

SchemaHelper.addVirtualId(ListSchema);
SchemaHelper.addToJsonOrObject(ListSchema);
SchemaHelper.addTransforms(ListSchema);

ListSchema.plugin(timestamps);

module.exports = mongoose.model('List', ListSchema);
