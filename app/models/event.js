'use strict';

const mongoose = require('./../middleware/db').mongoose;

const timestamps = require('mongoose-timestamp');

const Schema = mongoose.Schema;

const SchemaHelper = require('./../helpers/schema');

const statuses = ['accepted', 'rejected', 'tentative']

const EventSchema = new Schema({
  title: { type: String, required: true },
  image: { type: String },
  description: { type: String },
  master: { type: Schema.Types.ObjectId, ref: 'Event' },
  start: { type: Date, required: true },
  end: { type: Date },
  location: { type: String },
  mailingList: { type: Schema.Types.ObjectId, required: true, ref: 'List' },
  createdBy: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
  invitees: [{
    user: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    status: {
      type: String,
      required: true,
      enum: statuses
    },
    at: { type: Date, default: Date.now }
  }]
});

SchemaHelper.addVirtualId(EventSchema);
SchemaHelper.addToJsonOrObject(EventSchema);
SchemaHelper.addTransforms(EventSchema);

EventSchema.plugin(timestamps);

module.exports = mongoose.model('Event', EventSchema);
