'use strict';

const mongoose = require('./../middleware/db').mongoose;

const timestamps = require('mongoose-timestamp');

const Schema = mongoose.Schema;

const SchemaHelper = require('./../helpers/schema');

const emailRegex = /^.+@.+$/;

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    index: true,
    unique: true,
    lowercase: true,
    match: [emailRegex, 'Please enter a valid email']
  },
  name: { type: String }
});

SchemaHelper.addVirtualId(UserSchema);
SchemaHelper.addToJsonOrObject(UserSchema);
SchemaHelper.addTransforms(UserSchema);

UserSchema.plugin(timestamps);

module.exports = mongoose.model('User', UserSchema);
